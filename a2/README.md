

# LIS4381 - Mobile Web Application Development

## Megan Hubbard

### Assignment 2 Requirements:

*Three Parts:*

1. Healthy Recipe Mobile App
2. Chapter Questions (Chs 3, 4)
3. Bitbucket repo link

#### README.md file should include the following items:

* Screenshots of running mobile app
* Screenshots of Java skill sets


#### Assignment Screenshots:

*Screenshots of Healthy Recipes - Mobile App*:

| Splash Screen     | Ingredient Screen |
| -------------     | ----------------- |
|![](img/Splash.png)|![](img/Ing.png)   |

*Screenshots of Java Skill Sets*:

|Skill Set 1: Even Or Odd | Skill Set 2: Largest Integers | Skill Set 3: Arrays and Loops |
| --------------          | -------------------           | --------------------------    |
|![](img/EoO.png)         |![](img/Largest.png)           |![](img/Arrays.png)            |  