

# Course LIS4381

## Megan Hubbard

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My a1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My First App
    - Provide Screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My a2 README.md file")
    - Create Healthy Recipes Android App
    - Provide screenshots of completed app
    - Provide screenshots of completed Java skill sets


3. [A3 README.md](a3/README.md "My a3 README.md file")
    - Create ERD based upon business rules
    - Provide screenshot of completed ERD
    - Provide DB resource links

4. [A4 README.md](a4/README.md "My a4 README.md file")
    - TBD


5. [A5 README.md](a5/README.md "My a5 README.md file")
    - TBD


6. [P1 README.md](p1/README.md "My p1 README.md file")
    - Create My Business Card Android App
    - Provide screenshots of completed app
    - Provide screenshots of completed Java skill sets

