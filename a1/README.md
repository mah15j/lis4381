> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Megan Hubbard

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1, 2)
4. Bitbucket repo links: a) this assignment and b) the completed tutorial above (bitbucket stationlocations)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation
* Screenshot of running java Hello;
* Screenshot of running Android Studio - My First App
* git commands w/ short descriptions;

>
> #### Git commands w/short descriptions:

1. git init -  Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. get remote -v - List all currently configured remote repositories

#### Assignment Screenshots:

*Screenshot of AMPPS running [My PHP Installation](http://localhost:8080 "PHP Localhost")*:

![AMPPS Installation Screenshot](img/php.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mah15j/bitbucketstationlocations/ "Bitbucket Station Locations")


