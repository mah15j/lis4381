

# LIS4381 - Mobile Web Application Development

## Megan Hubbard

### Project 1 Requirements:

*Three Parts:*

1. My Business Card Mobile App
2. Chapter Questions (Chs 7, 8)
3. Bitbucket repo link

#### README.md file should include the following items:

* Screenshots of running mobile app
* Screenshots of Java skill sets


#### Assignment Screenshots:

*Screenshots of My Business Card - Mobile App*:

| "Me" Screen       | Details Screen    |
| -------------     | ----------------- |
|![](img/me.png)    |![](img/details.png)   |

*Screenshots of Java Skill Sets*:

|Skill Set 7: Random Array | Skill Set 8: Largest of Three | Skill Set 9: Array Runtime |
| --------------          | -------------------           | --------------------------    |
|![](img/arrayMethods.png)         |![](img/largestThree.png)           |![](img/arrayRuntime.png)            |  